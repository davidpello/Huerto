from flask import Flask, jsonify, request, Response, render_template
import datetime
import logging
import pymongo
import requests
import json
from influxdb import InfluxDBClient
from config import options

# configure app log
logging.basicConfig(filename = 'huerto.log', level=logging.DEBUG)
app = Flask(__name__)

# main route
@app.route("/")
def home():
    dt = datetime.datetime.now()
    return "<h1>" + dt.strftime("%A, %d. %B %Y %I:%M%p") + "</h1>"

# upload data route (from TTN webhook)
@app.route("/upttn", methods=["POST"])
def upttn():
    if request.method == 'POST':
        if request.headers.get("X-Shitty-Auth") != options["auth-key"]:
            return Response(response="Unauthorized", status=401)
        
        data = request.get_json()
        if data:
            # get values
            payload = data["uplink_message"]["decoded_payload"] 
            baro = payload["barometric_pressure_0"]
            temp = payload["temperature_0"]
            hum = payload["relative_humidity_0"]
            soil = payload["analog_in_0"]
            batt = payload["analog_in_1"]

            # insert it in mongodb
            insert_mongodb(baro, temp, hum, soil, batt)
                        
            # and influxdb
            insert_influxdb(baro, temp, hum, soil, batt)

            # tests
            data_tests(baro, temp, hum, soil, batt)

            # debug log
            app.logger.debug('TTNV3: ' + data["received_at"] + " : " + \
                             str(baro) + "," + str(temp) + "," + str(hum) + \
                             "," + str(soil) + "," + str(batt))

            return Response(response="OK", status=200)
        else:
            return Response(response="Bad request", status=400)

# get one list of data
@app.route("/data/<string:field>", defaults={'period': 1})
@app.route("/data/<string:field>/<int:period>")
def data(field, period):
    fields = ["temp", "baro", "soil", "hum", "batt"]
    if field not in fields:
        return Response(response="Bad request", status=400)

    # get timestamp for 'period' ago
    curr_date = datetime.datetime.now()
    diff = datetime.timedelta(weeks=period)
    #just one day
    if period == 0:
        diff = datetime.timedelta(days=1)
    search_timestamp = (curr_date - diff).timestamp()

    # get data
    dbclient = pymongo.MongoClient("mongodb://localhost:27017/")            
    db = dbclient[options["database"]]
    col = db[options["collection"]]

    data = col.find({'id': { '$gt': search_timestamp }})#.skip(col.count() - 100)
    
    datalist = []
    for d in data:
        datalist.append({"id" : d["id"], "date": d["date"], "data" : d[field]})

    # skip if needed
    if period > 0:
        skip = period * 4
        datalist = datalist[::skip]

    return jsonify(datalist)


# generate charts
@app.route("/charts")
def charts():
    return render_template('charts.html', config=options)

# bot commands
@app.route("/bot", methods=["POST"])
def bot():
    data = request.get_json()
    if data:
        #app.logger.debug("Telegram bot webhook" + json.dumps(data))
        # text message?
        if "text" in data["message"]:
            # command?
            if data["message"]["text"].startswith("/"):
                app.logger.debug("Telegram bot webhook command" + json.dumps(data))
                if "/status" in data["message"]["text"]:
                    last_data = get_mongodb_last()
                    telegram_bot_info(last_data["date"], last_data["baro"], \
                                      last_data["temp"], last_data["hum"], \
                                      last_data["soil"], last_data["batt"])
                else:
                    telegram_bot_text("🤷")
        return Response(response="ok", status=200)
    else:
        return Response(response="Bad request", status=400)
        


# databases
def insert_mongodb(baro, temp, hum, soil, batt):
    dbclient = pymongo.MongoClient("mongodb://localhost:27017/")            
    db = dbclient[options["database"]]
    col = db[options["collection"]]
    dt = datetime.datetime.now()
    dbdata = {"id": dt.timestamp(), \
              "date": dt.isoformat(), \
              "baro": baro, \
              "temp": temp, \
              "hum" : hum, \
              "soil": soil, \
              "batt": batt }

    return col.insert_one(dbdata)
            
def get_mongodb_last():
    dbclient = pymongo.MongoClient("mongodb://localhost:27017/")            
    db = dbclient[options["database"]]
    col = db[options["collection"]]

    data = col.find().sort("_id", -1).limit(1)
    app.logger.debug("LAST DATA: " + str(data[0]))
    return data[0]

def insert_influxdb(baro, temp, hum, soil, batt):
    influxclient = InfluxDBClient(host='localhost', port=8086)
    influxclient.switch_database(options["influxdb"])
    influxjson = [
        {
            "measurement": options["influxpoint"],
            "tags": {
                "user": "flaskapp"
            },
            "time": datetime.datetime.utcnow().isoformat(), #fluxdb use UTC
            "fields": {
                "baro": float(baro),
                "temp": float(temp),
                "hum": float(hum),
                "soil": float(soil),
                "batt": float(batt)
            }
        }
    ]
    return influxclient.write_points(influxjson)
    
# utils    
def data_tests(baro, temp, hum, soil, batt):
    if temp > options["max_temp"]:
        telegram_bot_alert("La temperatura", temp, "ºC", True)
    if temp < options["min_temp"]:
        telegram_bot_alert("La temperatura", temp, "ºC", False)

    if soil < options["min_soil"]:
        telegram_bot_alert("La humedad del suelo", soil, "%", False)
    #if soil > options["max_soil"]:
    #    telegram_bot_alert("La humedad del suelo", soil, "%", True)

    if batt < options["min_batt"]:
        telegram_bot_alert("La batería", batt, "V", False)


# telegram functions
def telegram_bot_info(date, baro, temp, hum, soil, batt):
    # format date
    dt = datetime.datetime.fromisoformat(date)
    formatted_date = dt.strftime("🗓️ %d/%m/%Y, 🕑 %H:%M")
    api_url = "https://api.telegram.org/bot" + options["telegram_token"]
    command = "/sendMessage?chat_id=" + options["telegram_channel"] + "&text="
    msg = formatted_date + "\n  🌡️: " + str(temp) + " ºC\n 💧: " + str(hum) + \
        " %\n 🌱: " + str(soil) + " %\n 🌦️: " + str(baro) + " mBar\n 🔋: " + \
        str(batt) + " V"

    response = requests.get(api_url + command + msg)
    
def telegram_bot_text(text):
    api_url = "https://api.telegram.org/bot" + options["telegram_token"]
    command = "/sendMessage?chat_id=" + options["telegram_channel"] + "&text="
    msg = text

    response = requests.get(api_url + command + msg)

def telegram_bot_alert(key, value, unit, bigger):
    api_url = "https://api.telegram.org/bot" + options["telegram_token"]
    command = "/sendMessage?chat_id=" + options["telegram_channel"] + "&text="
    msg = "⚠️ ALERTA ⚠️  " + key + (" es muy alta" if bigger else " es muy baja") + \
        " : " + str(value) + " " + str(unit)

    response = requests.get(api_url + command + msg)
